<?php
require_once('traits/RapidStrike.php');
require_once('traits/MagicShield.php');

class Orderus extends Character
{
    use RapidStrike {
        RapidStrike::__construct as RapidStrike;
    }

    use MagicShield {
        MagicShield::__construct as MagicShield;
    }

    public function __construct()
    {
        $this->health = rand(70, 100);
        $this->strength = rand(70, 80);
        $this->defense = rand(45, 55);
        $this->speed = rand(40, 50);
        $this->luck = rand(10, 30);
    }

    public function performAttackSkills()
    {
        parent::performAttackSkills();

        $this->RapidStrike();
    }

    public function performDefenseSkills()
    {
        parent::performDefenseSkills();

        $this->MagicShield();
    }
}