<?php
class ViewController
{
    public function __construct()
    {
    }

    public function display($view, $d = [])
    {
        require_once 'views/' . $view . '.php';
    }
}