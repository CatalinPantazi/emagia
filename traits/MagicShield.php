<?php
trait MagicShield
{
    public function __construct()
    {
        $this->chance = 20;

        if ($this->doesOccur()) {
            $battle = Battle::Instance();
            $battle->damage /= 2;
            $battle->logEvent('MagicShield skill was used');
            $battle->logEvent('Damage adjusted to ' . $battle->damage);
            $battle->defender->health += $battle->damage;
            $battle->logEvent(get_class($battle->defender) . ' health is ' . $battle->defender->health);
        }
    }
}