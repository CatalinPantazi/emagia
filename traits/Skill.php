<?php
trait Skill
{
    protected $chance;

    protected function doesOccur()
    {
        return rand(1, 100) <= $this->chance;
    }
}