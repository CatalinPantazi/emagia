<?php
spl_autoload_register(function ($class) {
    require_once 'models/' . $class . '.php';
});

require_once 'controllers/ViewController.php';

$orderus = new Orderus();
$beast = new Beast();

$battle = Battle::Instance();
$battle->initBattle($orderus, $beast);
$battle->doBattle();

$view = new ViewController();
$view->display('page', $battle->getOutcome());