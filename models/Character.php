<?php
require_once('traits/Skill.php');

abstract class Character
{
    use Skill;

    public $health;
    public $strength;
    public $defense;
    public $speed;
    public $luck;

    public function performAttackSkills()
    {

    }

    public function performDefenseSkills()
    {

    }
}