<?php
trait RapidStrike
{
    public function __construct()
    {
        $this->chance = 10;

        if ($this->doesOccur()) {
            $battle = Battle::Instance();
            $battle->logEvent('RapidStrike skill was used');
            $battle->performStrike();
        }
    }
}