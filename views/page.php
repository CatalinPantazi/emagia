<link rel="stylesheet" href="assets/style.css" />

<ul id="players">
<?php foreach ($d['players'] as $name => $stats) { ?>
    <li>
        <h1><?=$name?></h1>
        <?php foreach ($stats as $stat => $val) { ?>
            <div><div style="width:<?=$val?>%"><?=$stat?> <?=$val?></div></div>
        <?php } ?>
    </li>
<?php } ?>
</ul>

<ul id="events">
<?php foreach ($d['log'] as $turn => $events) { ?>
    <li>
        <h2>Turn <?=$turn?></h2>
        <?=implode('<br/>', $events)?>
    </li>
<?php } ?>
</ul>

<?php
echo '<pre>';
if ($d['winner'] == 'Orderus') {
    echo '
   ____          _                                           
  / __ \        | |                                          
 | |  | |_ __ __| | ___ _ __ _   _ ___  __      _____  _ __  
 | |  | | \'__/ _` |/ _ \ \'__| | | / __| \ \ /\ / / _ \| \'_ \ 
 | |__| | | | (_| |  __/ |  | |_| \__ \  \ V  V / (_) | | | |
  \____/|_|  \__,_|\___|_|   \__,_|___/   \_/\_/ \___/|_| |_|
                                                             
                                                             
';
} else {
    echo '
  ____                 _                         
 |  _ \               | |                        
 | |_) | ___  __ _ ___| |_  __      _____  _ __  
 |  _ < / _ \/ _` / __| __| \ \ /\ / / _ \| \'_ \ 
 | |_) |  __/ (_| \__ \ |_   \ V  V / (_) | | | |
 |____/ \___|\__,_|___/\__|   \_/\_/ \___/|_| |_|
                                                 
                                                 
';
}