<?php
final class Battle
{
    private $players;
    public $attacker;
    public $defender;

    private $turn = 0;
    private $maxTurns = 20;
    public $damage;

    private $battleLog;

    private  function __construct()
    {

    }

    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new Battle();
        }
        return $inst;
    }

    public function initBattle(Character $player1, Character $player2)
    {
        $this->players[get_class($player1)] = get_object_vars($player1);
        $this->players[get_class($player2)] = get_object_vars($player2);

        $this->attacker = $player1;
        $this->defender = $player2;

        if (
            ($this->defender->speed > $this->attacker->speed)
            || ($this->defender->speed == $this->attacker->speed && $this->defender->luck > $this->attacker->luck)
        ) {
            $this->swapRoles(false);
        }

        $this->turn = 1;
    }

    public function doBattle()
    {
        while ($this->isOngoing()) {
            $this->performStrike();
            $this->attacker->performAttackSkills();
            $this->defender->performDefenseSkills();
            $this->swapRoles();
        }
    }

    private function isOngoing()
    {
        return $this->turn <= $this->maxTurns && $this->attacker->health > 0 && $this->defender->health > 0;
    }

    public function performStrike()
    {
        $this->logEvent(get_class($this->attacker) . ' is attacking ' . get_class($this->defender));
        $this->logEvent(get_class($this->defender) . ' health is ' . $this->defender->health);

        if ($this->isDefenderLucky()) {
            $this->damage = 0;
        } else {
            $this->damage = $this->attacker->strength - $this->defender->defense;
        }
        $this->takeDamage();
    }

    private function takeDamage()
    {
        $this->defender->health -= $this->damage;

        $this->logEvent('Damage is ' . $this->damage . ($this->damage == 0 ? ', the defender got lucky' : ''));
        $this->logEvent(get_class($this->defender) . ' health is ' . $this->defender->health);
    }

    private function isDefenderLucky()
    {
        return rand(1, 100) <= $this->defender->luck;
    }

    private function swapRoles($advanceTurn = true)
    {
        list($this->attacker, $this->defender) = array($this->defender, $this->attacker);
        if ($advanceTurn)
            ++$this->turn;
    }

    public function logEvent($message)
    {
        $this->battleLog[$this->turn][] = $message;
    }

    private function getWinner()
    {
        return $this->attacker->health > $this->defender->health ? $this->attacker : $this->defender;
    }

    public function getOutcome()
    {
        return [
            'players' => $this->players,
            'winner' => get_class($this->getWinner()),
            'log' => $this->battleLog
        ];
    }
}